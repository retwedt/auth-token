#!/usr/bin/env node

/**
 * Generate a token from AWS Cognito.
 */

// Imports
const Auth = require("aws-amplify").Auth;
global.fetch = require("node-fetch");

const process = require("process");
const program = require("commander");

const clipboard = require("clipboardy");

// Configure command line arguments using commander.
// TODO(rex): Make the default values configurable, possible use a separate command and file that can be overwritten.
program
  .version("0.1.0")
  .option(
    "-u, --username <string>",
    "Registered email for your AWS Cognito service. Use single quotes (') if your username includes special characters."
  )
  .option(
    "-p, --password <string>",
    "Registered password for your AWS Cognito account. Use single quotes (') if your password includes special characters."
  )
  .option(
    "-r, --region [string]",
    "The region for your AWS Cognito service.",
    "us-east-1"
  )
  .option(
    "-i, --identity-pool [string]",
    "The Identity Pool ID for your AWS Cognito service.",
    "us-east-1:f1e3c5bd-84f1-4d35-8387-12f0d264b91b"
  )
  .option(
    "-s, --user-pool [string]",
    "The ID for your AWS User Pool.",
    "us-east-1_HrBEEnfE8"
  )
  .option(
    "-a, --app-client [string]",
    "The App Client ID for your AWS User Pool.",
    "3g2jescbo1kb2kqv2t9k27e7qg"
  )
  .parse(process.argv);

// Get values from command line.
const {
  username,
  password,
  region,
  userPool: userPoolId,
  identityPool: identityPoolId,
  appClient: userPoolAppClientId
} = program;

if (!username || !password) {
  console.error("A username and password are required for this app!");
  process.exit(1);
}

console.log(
  `AWS Config:
    Region: ${region}
    User Pool ID: ${userPoolId}
    Identity Pool ID: ${identityPoolId}
    User Pool App Client Id: ${userPoolAppClientId}`
);

console.log(
  `Generating token for:
    User: ${username}
    Password: ${password}`
);

// Construct the awsConfig object, and configure the Amplify.Auth library.
const awsConfig = {
  Auth: {
    identityPoolId,
    mandatorySignIn: true,
    region,
    userPoolId,
    userPoolWebClientId: userPoolAppClientId
  }
};
Auth.configure(awsConfig);

// Generate a token, then print it to the console.
Auth.signIn(username, password)
  .then(response => {
    const token = response.signInUserSession.idToken.jwtToken;
    console.log(`Token: ${token}`);

    // Copy the token to the clipboard.
    clipboard.writeSync(token);
    console.log(`Token copied to clipboard!`);
    process.exit(0);
  })
  .catch(err => {
    console.error(`Error generating token: ${err.message}`);
    process.exit(1);
  });
